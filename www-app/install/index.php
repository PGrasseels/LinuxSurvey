<?php
  /*if(file_exists('../conf/conf.php')){
    header('Location: ../');
  }*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Dashboard - Metro King</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">


  <!-- Stylesheets -->
  <link href="style/bootstrap.css" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="../style/font-awesome.css"> 
  <!-- jQuery UI -->
  <link rel="stylesheet" href="../style/jquery-ui.css"> 
  <!-- Calendar -->
  <link rel="stylesheet" href="../style/fullcalendar.css">
  <!-- prettyPhoto -->
  <link rel="stylesheet" href="../style/prettyPhoto.css">   
  <!-- Star rating -->
  <link rel="stylesheet" href="../style/rateit.css">
  <!-- Date picker -->
  <link rel="stylesheet" href="../style/bootstrap-datetimepicker.min.css">
  <!-- jQuery Gritter -->
  <link rel="stylesheet" href="../style/jquery.gritter.css">
  <!-- CLEditor -->
  <link rel="stylesheet" href="../style/jquery.cleditor.css"> 
  <!-- Bootstrap toggle -->
  <link rel="stylesheet" href="../style/bootstrap-switch.css">
  <!-- Main stylesheet -->
  <link href="../style/style.css" rel="stylesheet">
  <!-- Widgets stylesheet -->
  <link href="../style/widgets.css" rel="stylesheet">   
 
  
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="../img/favicon/favicon.png">
</head>

<body>


<div class="content">

  	<!-- Main bar -->
  	<div class="mainbar">
      
	    <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	        <h2 class="pull-left">Dashboard 
			  <!-- page meta -->
			  <span class="page-meta">Something Goes Here</span>
			</h2>


			<!-- Breadcrumb -->
			<div class="bread-crumb pull-right">
			  <a href="index.html"><i class="icon-home"></i> Home</a> 
			  <!-- Divider -->
			  <span class="divider">/</span> 
			  <a href="#" class="bread-current">Dashboard</a>
			</div>

			<div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->

         
            </div>

        </div>
         

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

<!-- JS -->
<script src="../js/jquery.../js"></script> <!-- jQuery -->
<script src="../js/bootstrap.../js"></script> <!-- Bootstrap -->
<script src="../js/jquery-ui-1.10.2.custom.min.../js"></script> <!-- jQuery UI -->
<script src="../js/fullcalendar.min.../js"></script> <!-- Full Google Calendar - Calendar -->
<script src="../js/jquery.rateit.min.../js"></script> <!-- RateIt - Star rating -->
<script src="../js/jquery.prettyPhoto.../js"></script> <!-- prettyPhoto -->

<!-- jQuery Flot -->
<script src="../js/excanvas.min.../js"></script>
<script src="../js/jquery.flot.../js"></script>
<script src="../js/jquery.flot.resize.../js"></script>
<script src="../js/jquery.flot.pie.../js"></script>
<script src="../js/jquery.flot.stack.../js"></script>

<script src="../js/jquery.gritter.min.../js"></script> <!-- jQuery Gritter -->
<script src="../js/sparklines.../js"></script> <!-- Sparklines -->
<script src="../js/jquery.cleditor.min.../js"></script> <!-- CLEditor -->
<script src="../js/bootstrap-datetimepicker.min.../js"></script> <!-- Date picker -->
<script src="../js/bootstrap-switch.min.../js"></script> <!-- Bootstrap Toggle -->
<script src="../js/filter.../js"></script> <!-- Filter for support page -->
<script src="../js/custom.../js"></script> <!-- Custom codes -->
<script src="../js/charts.js"></script> <!-- Custom chart codes -->

</body>
</html>