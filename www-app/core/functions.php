<?php

// Retourne la distrib
function distrib()
{
	$fic = file('/proc/version');
	
	list($OS, $version, $kernel, $distrib1, $distrib1_v, $mail, $gcc, $gcc_v, $gcc_vv, $gcc_date, $gcc_real, $distrib2, $distrib2_v) = explode(' ', $fic[0]);
	
	list($distrib_vers) = explode('.', $distrib2_v);
	$distrib = substr($distrib2, 1).' '.$distrib_vers;
	
	return $distrib;
}


// Retourne un uname -a modifié
// Ex. : echo aff_sysname(); => Linux (Debian 4) 2.6.18-4-486 (i686)
function aff_sysname()
{
	$return['kernel'] = php_uname('s').' ('.distrib().') ';
	$return['kernel']  .= php_uname('r').' ';
	$return['kernel']  .= '('.php_uname('m').')';
	$return['os'] = php_uname('v');
	$return['hote'] = php_uname('n');
	$return['uptime'] = uptime();
	
	return $return;
}

// Retourne l'uptime
// echo uptime();
function uptime()
{
	$result = NULL;
	
	$fd = fopen('/proc/uptime', 'r');
	$ar_buf = split(' ', fgets($fd, 4096));
	fclose($fd);

	$sys_ticks = trim($ar_buf[0]);
	$min = $sys_ticks / 60;
	$heures = $min / 60;
	$jours = floor($heures / 24);
	$heures = floor($heures - ($jours * 24));
	$min = floor($min - ($jours * 60 * 24) - ($heures * 60));
	
	$text['jours'] = 'jours';
	$text['heures'] = 'heures';
	$text['minutes'] = 'minutes';

	if ($jours != 0)
	{
		if ($jours < 2) $text['jours'] = 'jour';
		$result .= $jours.' '.$text['jours'].' ';
	}

	if ($heures != 0)
	{
		if ($heures < 2) $text['heures'] = 'heure';
   		$result .= $heures.' '.$text['heures'].' ';
	}

	if ($min < 2) $text['minutes'] = 'minute';

	$result .= $min.' '.$text['minutes'];

	return $result;
}


?>