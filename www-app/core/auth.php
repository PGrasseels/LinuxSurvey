<?php

include('./../conf/conf.php');

$data = array();

if(AUTH){
	if(empty($_POST['password'])){
		$data['error'] = true;
		$data['errorMessage'] = 'Password required';
	}else{
		if($_POST['password'] == PASSWORD){
			$data['error'] = false;
		}else{
			$data['error'] = true;
			$data['errorMessage'] = 'Password error';
		}
	}
}else{
	$data['error'] = false;
}

echo json_encode($data);

?>